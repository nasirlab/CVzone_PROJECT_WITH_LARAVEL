@extends('admin.layouts.master')
@section('portfolios_menu_add','active')
@section('pageTitle')
<span class="text-semibold">PORTFOLIOS - ADD</span>  || <a href="/portfolios">MY PORTFOLIOS</a>
@endsection

@section('content')
<div class="row">
	<form action="store.php" method="POST" enctype="multipart/form-data">
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
						<h5>Please add your portfolios here .</h5>
							<div class="form-group">
								<label>Project Title</label>
								<input class="form-control" type="text" placeholder="Web Design" name="title">
							</div>	
							<div class="form-group">
								<label>Project image</label>
								<input class="form-control" type="file" name="img" >
							</div>														
							<div class="form-group">
								<label>Category</label>
								<input class="form-control" type="text" placeholder="Logo design" name="category">

							</div>								
							<div class="form-group">
								<label>Project description</label>
								<textarea class="form-control input-xlg" name="description" ></textarea>
							</div>

							<div class="form-group">
								<input class="input-xs" type="submit" value="Save" name="protfolios">

							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
 </form>
</div>
@endsection