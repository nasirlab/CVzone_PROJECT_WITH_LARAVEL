@extends('admin.layouts.master')
@section('portfolios_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY PORTFOLIOS</span>  || <a href="/portfolios/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="7">
									 <h3 class='text-center'>Protfolio information</h3>
									
									</th>
								</tr>				
								<tr>
									<th>Sl no.</th>
									<th>Project Image</th>
									<th>Project Title</th>
									<th>Ctegory</th>
									<th>Description</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>

									<tr>
										<td> 01</td>
										<td> <img width="90px" height="70" src="../../../assets/images/"> 
										</td>
										<td>Title</td>
										<td>category</td>
										<td>
											<p class="text-justify">...</p>
										</td>
										<td>
											<a class="btn-success" href="/portfolios/show">View</a>
										</td>										
										<td>
											<a class="btn-success" href="/portfolios/edit">Edit</a>
										</td>
										<td>
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/portfolios/trash">Delete</a> 
										</td>
									</tr>								

							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection