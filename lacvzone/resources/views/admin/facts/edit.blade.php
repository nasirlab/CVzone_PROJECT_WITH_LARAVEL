@extends('admin.layouts.master')
@section('facts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">FACTS - EDIT</span>  || <a href="/facts"> MY FACTS</a> || <a href="/facts/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row" >
	<form action="update.php" method="POST" enctype="multipart/form-data">
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
							<h5>You can update your facts .</h5>;
							<div class="form-group">
								<label>Facts Title</label>
								<input class="form-control input-xlg" type="text" value="Title" name="title">
							</div>														
							<div class="form-group">
								<label>Facts number</label>
								<input class="form-control input-xlg" type="text" value="35" name="no_of_items">
							</div>
							<div class="form-group">
								<label>Facts image</label>
								<input class="form-control input-xlg" type="file" name="img" >
							</div>	
							<div class="form-group">
								<input class="input-xs" type="hidden" value="" name="id">
								<input class="input-xs" type="submit" value="Update" name="facts">

							</div>

						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>
@endsection