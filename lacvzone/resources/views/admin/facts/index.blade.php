@extends('admin.layouts.master')
@section('facts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY FACTS</span>  || <a href="/facts/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
		<div class="table-responsive">
				<table class="table bg-slate-600">
					<thead>
						<tr>
						<th colspan="5">
							<h2 class='text-center'>My Facts</h2>
						</th>
						</tr>				
						<tr>
							<th>Sl no.</th>
							<th>Facts Image</th>
							<th>Facts Title</th>
							<th>Facts Number</th>
							<th colspan="2">Manage</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> 01 </td>
							<td> <img width="90px" height="70" src="../../../assets/images/" alt="Image"> </td>
							<td>Facts Title</td>
							<td>35</td>
							<td>
								<a class="btn-success" href="/facts/edit">Edit</a> ||

								<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/facts/trash">Delete</a> 
							</td>
						</tr>								
					</tbody>
				</table>
		</div>
	</div>
</div>		
@endsection