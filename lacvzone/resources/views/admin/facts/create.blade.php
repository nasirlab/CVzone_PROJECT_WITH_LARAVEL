@extends('admin.layouts.master')
@section('facts_menu_add','active')
@section('pageTitle')
<span class="text-semibold">FACTS - ADD</span>  || <a href="/facts"> MY FACTS</a>
@endsection

@section('content')
<div class="row">
	<form action="store.php" method="POST" enctype="multipart/form-data">
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
						<h5>Please add your facts here .</h5>
							<div class="form-group">
								<label>Facts Title</label>
								<input class="form-control input-xlg" type="text" placeholder="Web Design" name="title">
							</div>	
							<div class="form-group">
								<label>Facts image</label>
								<input class="form-control input-xlg" type="file" name="img" >
							</div>														
							<div class="form-group">
								<label>Facts number</label>
								<input class="form-control input-xlg" type="text" placeholder="52" name="no_of_items">
								
							</div>
							<div class="form-group">
								<input class="input-xs" type="submit" value="Save" name="facts">

							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>	
@endsection