@extends('admin.layouts.master')
@section('hobbies_menu_add','active')
@section('pageTitle')
<span class="text-semibold">HOBBIES - ADD</span>  | <a href="/hobbies"> MY HOBBIES</a>
@endsection

@section('content')
	<div class="row ">
		<!-- Add hobbies -->
		<form action="store.php" method="POST" enctype="multipart/form-data">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">

								<div class="form-group">
									<label>Hobbies title</label>
									<input class="form-control input-xlg" type="text" placeholder="Fishing" name="title">
								</div>
								<div class="form-group">
									<label>Hobbies image</label>
									<input class="form-control input-xlg" type="file" placeholder="XLarge input height" name="img">
								</div>					
								<div class="form-group">
									<label>Hobbies description</label>
									<textarea class="form-control input-xlg" name="description"></textarea>

								</div>
								<div class="form-group">
									<input class="input-xs" type="submit" value="Save" name="hobbies">
								</div>
							</div>
							</div>
						</div>
				</div>
			</fieldset>
		</form>
	 </div>
  </div>	
@endsection