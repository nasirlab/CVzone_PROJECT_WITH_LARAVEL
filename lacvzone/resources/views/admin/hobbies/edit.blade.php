@extends('admin.layouts.master')
@section('hobbies_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">HOBBIES - EDIT</span>  | <a href="/hobbies"> MY HOBBIES</a> | <a href="/hobbies/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row ">
	<!-- Edit hobbies -->
	<form action="update.php" method="POST" enctype="multipart/form-data">
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10">
					<div class="row">
						<div class="col-md-6 col-md-offset-1">
							<h2>You can Edit your hobbies .</h2>
							<div class="form-group">
								<label>Hobbies title</label>
								<input class="form-control input-xlg" type="text" value="Title" name="title">
							</div>									
							<div class="form-group">
								<label>Hobbies description</label>
								<input class="form-control input-xlg" type="text" value="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt, alias." name="description">
							</div>								
							<div class="form-group">
								<label>Hobbies image</label>
								<input class="form-control input-xlg" type="file" value="" name="img">
								<img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/" alt="No Image">
									
							</div>					
							<div class="form-group">
								<input class="input-xs" type="hidden" value="" name="id">
								<input class="input-xs" type="submit" value="Update" name="hobbies">

							</div>
						</div>
						</div>
					</div>
			</div>
		</fieldset>
	</form>

	 </div>
 </div>
 @endsection	