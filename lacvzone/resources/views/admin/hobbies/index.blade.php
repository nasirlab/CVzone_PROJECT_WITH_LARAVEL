@extends('admin.layouts.master')
@section('hobbies_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY HOBBIES</span>  | <a href="/hobbies/create"> ADD NEW</a>
@endsection

@section('content')
<div class="row">
		<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
			<div class="table-responsive">
					<table class="table bg-slate-600">
						<thead>
							<tr>
								<th colspan="4">
								</th>
							</tr>				
							<tr>
								<th>Image</th>
								<th>Title</th>
								<th>Description</th>
								<th colspan="2">Manage</th>
							</tr>
						</thead>
						<tbody>


									<tr>
										<td><img width="90" height="70" src="../../../assets/images/" alt="No Image"> 
										</td>
										<td>
											Hobbies title
										</td>
										<td><p class="text-justify">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate facilis placeat necessitatibus eum qui voluptas ex rerum ad harum! Autem soluta placeat magnam incidunt, tenetur necessitatibus neque provident deserunt aliquam?
										</p></td>
										<td>
											<a class="btn-success" href="/hobbies/edit">Edit</a>
										</td>
										<td>	
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/hobbies/trash">Delete</a> 
										</td>
									</tr>									
						</tbody>
					</table>
			</div>
	 </div>
</div>	
@endsection