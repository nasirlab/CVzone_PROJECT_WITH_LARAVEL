@extends('admin.layouts.master')
@section('settings_menu_manage','active')
 


@section('pageTitle')
<span class="text-semibold">SETTINGS - EDIT</span>
@endsection

@section('content')
<div class="row">
	<!--Setting options code-->
	<form action="update.php" method="POST" enctype="multipart/form-data">
		<fieldset class="content-group">
		<div class="form-group">
			<div class="col-lg-10">
				<div class="row">
					<div class="col-md-6 col-md-offset-1">
						
						<div class="form-group">
							<label>Profile Picture</label>
							<input class="form-control" type="file"  name="featured_img">
						</div>					
						<div class="form-group">
							<label>Your title</label>
							<input class="form-control" type="text" value="Wev developer" name="title">
						</div>

						<div class="form-group">
							<label>Your name</label>
							<input class="form-control" type="text" value="Md Nasir Fardoush" name="fullname">
						</div>
						<div class="form-group">
						<label>Curent Location</label>
							<input class="form-control input-sm" type="text" value="Gazipure" name="address">
						</div>
						<div class="form-group">
							<h3>Chose your Theme color</h3>
							<input type="radio"  value="blue" name="themecolor">
							<label>Blue</label> 
							<input type="radio"  value="brown" name="themecolor">
							<label>Brown</label>
							<input type="radio"  value="cyan" name="themecolor">
							<label>Cyan</label>
							<input type="radio" value="light-green" name="themecolor">
							<label>Light Green</label>						
							<input type="radio" value="orange" name="themecolor">
							<label>Orange</label>
							<input type="radio"  value="green" name="themecolor">
							<label>Default</label>

						</div>
						<div class="form-group">
							<input class="input-xs" type="hidden" value="" name="id">
							<input class="input-xs" type="submit" value="Save new change">
						</div>
					</div>				
					<div class="col-md-4 col-lg-4 ">
						<h1></h1>
						<img width="100" height="90" src="../../../assets/images/" alt="Profile Image!!!" >
					</div>	
				</div>
			</div>
		</fieldset>
	</form>
	<!--Setting options code-->
</div>	
@endsection