@extends('admin.layouts.master')
@section('awards_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">AWARDS - EDIT</span>  || <a href="/awards">MY AWARDS</a> || <a href="/awards/create">ADD NEW</a>
@endsection

@section('content')
<div class="row ">
    <!-- about basic info about module -->
	<form action="update.php" method="POST">
		<fieldset class="content-group">
			<div class="form-group">
				<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
					<div class="row">
						<h5>You can update your awards.</h5>
						<!-- section one -->
						<div class="col-md-5">
							<div class="form-group">
								<label>Awards Title</label>
								<input class="form-control input-xlg" type="tel" value="Title" name="title">
							</div>			
							<div class="form-group">
								<label>Awards Year</label>
								<input id="datepicker" class="form-control" type="text" value="" name="year">
							</div>										
							<div class="form-group">
								<label>Sort description</label>
								<textarea class="form-control"  placeholder="" name="description">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni cupiditate ipsa, id, quo quaerat facilis perferendis tempora iure quam, placeat quia odit commodi at minus expedita? Quasi ipsum voluptatibus, quaerat.
								</textarea>
							</div>									
						</div>								
						<!-- Second section -->							
						<div class="col-md-5">
							<div class="form-group">
								<label>Organaizations Name</label>
								<input class="form-control input-xlg" type="text" value="" name="organization">
							</div>					
							<div class="form-group">
								<label>Organaization Location</label>
								<input class="form-control input-xlg" type="text" value="" name="location">
							</div>					
						</div>
					</div>
					
					<div class="form-group">
						<input  type="hidden" value="" name="id">
						<input class="marg-top" type="submit" value="Update" name="awards">
					</div>
				</div>
			</div>
		</fieldset>
	</form>	
</div>
@endsection