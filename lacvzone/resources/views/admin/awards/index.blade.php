@extends('admin.layouts.master')
@section('awards_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY AWARDS</span>  || <a href="/awards/create">ADD NEW</a>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
		<div class="table-responsive">
				<table class="table bg-slate-600">
					<thead>
						<tr>
							<th colspan="8"><h2 class="text-center">Awards Information</h2></th>			
						</tr>				
						<tr>
							<th>Sl no</th>
							<th>Title</th>
							<th>Description</th>
							<th>Organization</th>
							<th>Location</th>
							<th>Awards year</th>
							<th colspan="2">Manage</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>01</td>
							<td>Title</td>
							<td>
								<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem ratione possimus voluptatem quo reprehenderit dignissimos, consectetur eveniet optio culpa omnis.</p>
							</td>
							<td>DU</td>
							<td>Dhaka</td>
							<td>2013</td>
							<td>
								<a class="btn-success" href="/awards/edit">Edit</a> 
							</td>									
							<td>
								<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/awards/trash">Delete</a> 
							</td>
						</tr>									 	
					</tbody>
				</table>
		</div>
 </div>
</div>
@endsection