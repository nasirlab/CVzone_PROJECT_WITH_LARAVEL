@extends('admin.layouts.master')
@section('educations_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">EDUCATIONS - EDIT</span>  || <a href="/educations">MY EDUCATIONS</a> || <a href="/educations/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row ">
		    <!-- Update your educations -->
			<form action="update.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Educations Title</label>
										<input class="form-control input-xlg" type="tel" value="Electronics engg" name="title">
									</div>					
									<div class="form-group">
										<label>Institute Name</label>
										<input class="form-control" type="text" value="DU" name="institute">
									</div>

									<div class="form-group">
										<label>Enrolled Year</label>
										<input id="datepicker" class="form-control" type="text" value="2013" name="enrolled_year">
									</div>									
									<div class="form-group">
										<label>Result</label>
										<input class="form-control" type="text" value="3.80" name="result">
										<small>Enter result following GPA Standart</small>
									</div>									
									<div class="form-group">
										<label>Board</label>
										<input class="form-control" type="text" value="NA" name="education_board">
									</div>
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Educations Degree</label>
										<input class="form-control" type="tel" value="Bsc" name="degree">
										<small>Please write sort name.</small>
									</div>					
									<div class="form-group">
										<label>Institute Location</label>
										<input class="form-control" type="text" value="Dhaka" name="location">
									</div>

									<div class="form-group">
										<label>Passing Year</label>
										<input id="datepicker2" class="form-control" type="text" value="2016" name="passing_year">
									</div>									
									<div class="form-group">
										<label>Course duration(Years)</label>
										<input class="form-control" type="text" value="4" name="course_duration">
									</div>																	
								</div>
							</div>
							<div class="form-group">
								<input class="marg-top" type="hidden" value="" name="id">
								<input class="marg-top" type="submit" value="Update" name="education">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
@endsection