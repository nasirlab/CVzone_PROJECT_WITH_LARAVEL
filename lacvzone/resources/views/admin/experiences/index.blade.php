@extends('admin.layouts.master')
@section('experiences_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">MY EXPERIENCE</span>  || <a href="/experiences/create">ADD NEW</a>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
	<div class="table-responsive">
			<table class="table bg-slate-600">
				<thead>
					<tr>
						<th colspan="9"><h2 class="text-center">Experiences Information</h2></th>					
					</tr>				
					<tr>
						<th>Sl no</th>
						<th>Title</th>
						<th>Description</th>
						<th>Company</th>
						<th>Company location</th>
						<th>Start  year</th>
						<th>End year</th>
						<th colspan="2">Manage</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>01</td>
						<td>Developer</td>
						<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio, doloremque!</td>
						<td>Webtech</td>
						<td>Dhaka</td>
						<td>2015</td>
						<td>2016</td>
						<td>
							<a class="btn-success" href="/experiences/edit">Edit</a> 
						</td>									
						<td>
							<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/experiences/trash">Delete</a> 
						</td>
					</tr>							
				</tbody>
			</table>
	  </div>
   </div>
</div>	
@endsection