@extends('admin.layouts.master')
@section('skills_menu_add','active')
@section('pageTitle')
<span class="text-semibold">SKILLS - ADD</span>  || <a href="/skills">MY SKILLS</a>
@endsection

@section('content')
	<div class="row ">
		    <!-- Teaching Module -->
			<form action="store.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
							<h5>Please add your skills heare .</h5>
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Title</label>
										<input class="form-control" type="title" placeholder="Programming" name="title">
									</div>	
									<div class="form-group">
										<label>Skills area</label>
										<textarea class="form-control"  placeholder="PHP,AJAX,HTML" name="experience_area"></textarea>
										<small>Please separet your skills with comma(,)</small>
									</div>									
									<div class="form-group">
										<label>Sort description</label>
										<textarea class="form-control"  placeholder="Good command on this side" name="description"></textarea>
									</div>
								</div>																		
									<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Expreince(Years)</label>
										<input class="form-control" type="text" placeholder="3" name="experience">
									</div>						
								     <div class="form-group">
											<label>Skills level</label>
											<select class="form-control" name="level">
												<option value="Intermediate">Intermediate</option>
												<option value="Experts">Experts</option>
												<option value="Advance">Advance</option>
												<option value="Master">Master</option>
											</select>
								     	</div>										
								     	<div class="form-group">
											<label>Skills category</label>
											<select class="form-control" name="category" >
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
											</select>
								     	</div>						
								</div>
							</div>
							<div class="form-group">
									<input class="marg-top" type="submit" value="Save" name="teaching">
							</div>
						</div>
					</div>	
				</fieldset>
			</form>	
   		 </div>
@endsection