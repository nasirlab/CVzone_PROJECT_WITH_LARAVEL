@extends('admin.layouts.master')
@section('services_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">SERVICES - EDIT</span>  || <a href="/services">MY SERVICES</a> || <a href="/services/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row ">
			<form action="update.php" method="POST"  enctype="multipart/form-data">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<!-- section one -->
								<div class="col-md-5">
								<h6>Please write your services here.</h6>
									<div class="form-group">
										<label>Title</label>
										<input class="form-control " type="text" value="" name="title">
									</div>												
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control"  name="description">
											
										</textarea>
									</div>										
									<div class="form-group">
										<label>Integreted topics</label>
										<textarea class="form-control"  name="topics">
											
										</textarea>
										<small>Please topics devited by comma(,)</small>
									</div>
									<div class="form-group">
										<label>Servicess feture image</label>
										<input  class="form-control" type="file" placeholder="Rahim" name="img">
										<img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/" alt="No Image">
								</div>										
								</div>
								<!-- Second section -->							
								<div class="col-md-5">
								<h6>Please write your client feedback here.</h6>
									<div class="form-group">
										<label>Feedback</label>
										<textarea class="form-control input-xlg" type="text"  name="clinte_feedback">
											
										</textarea>
									</div>
									<div class="form-group">
										<label>Client image</label>
										<input class="form-control input-xlg" type="file"  name="client_image">
										<img style="margin-top: 15px;" width="90" height="70" src="../../../assets/images/" alt="No Image">
									</div>																
								</div>								
							</div>

							<div class="form-group">
								<input  type="hidden" value="" name="id">
								<input class="marg-top" type="submit" value="Update" name="services">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 
@endsection