@extends('admin.layouts.master')
@section('services_menu_add','active')
@section('pageTitle')
<span class="text-semibold">SERVICES - ADD</span>  || <a href="/services">MY SERVICES</a>
@endsection

@section('content')
	<div class="row ">
			<form action="store.php" method="POST"  enctype="multipart/form-data">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<!-- section one -->
								<div class="col-md-5">
								<h6>Please write your services here.</h6>
									<div class="form-group">
										<label>Title</label>
										<input class="form-control " type="text" placeholder=" Design Services" name="title">
									</div>	
									<div class="form-group">
										<label>Servicess feture image</label>
										<input  class="form-control" type="file" placeholder="Rahim" name="img">
									</div>												
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control"  placeholder="" name="description"></textarea>
									</div>										
									<div class="form-group">
										<label>Integreted topics</label>
										<textarea class="form-control"  placeholder="web design,Logodesign" name="topics"></textarea>
										<small>Please topics devited by comma(,)</small>
									</div>									
								</div>
								<!-- Second section -->							
								<div class="col-md-5">
								<h6>Please write your client feedback here.</h6>
									<div class="form-group">
										<label>Client image</label>
										<input class="form-control input-xlg" type="file" name="client_image">
									</div>					
									<div class="form-group">
										<label>Feedback</label>
										<textarea class="form-control input-xlg" type="text" placeholder="client feedback" name="clinte_feedback"></textarea>
									</div>																
								</div>								
							</div>

							<div class="form-group">
								<input class="marg-top" type="submit" value="Save" name="services">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>	
@endsection