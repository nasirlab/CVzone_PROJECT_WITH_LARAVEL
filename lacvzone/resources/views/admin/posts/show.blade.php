@extends('admin.layouts.master')
@section('posts_menu_manage','active')
@section('pageTitle')
<span class="text-semibold">POSTS - VIEW DETAILS</span> || <a href="/posts">MY POSTS</a> || <a href="/posts/create">ADD NEW</a>
@endsection

@section('content')
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="10"><h2 class="text-center">Post Details</h2></th>
								</tr>				
								<tr>
									<th>Image</th>
									<th>Title</th>
									<!-- <th>Description</th> -->
									<th>Category name</th>
									<th>Tags</th>
									<th>Author name</th>
									<th>City</th>
									<th>Country</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
									<img width="90" height="70" src="../../../assets/images/" alt="No Image"> 
									</td>
									<td>Title</td>
									<td>categories</td>
									<td>tags</td>
									<td>author_name</td>
									<td>city_name</td>
									<td>country_name</td>
								</tr>
								<tr>
									<td  colspan="7">
										<label>Post Description :</label>
										<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde voluptate vel vitae repudiandae nam nulla, eveniet quam neque nobis, voluptates asperiores natus labore maxime sapiente tempora quasi quia quis provident sunt excepturi blanditiis! Explicabo nulla dolorem alias accusamus quaerat culpa.</p>
									</td>									
								</tr>
								<tr>
									<td colspan="7">									
										<a class="btn-success" href="/posts/edit">Edit</a> ||
										<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="/posts/trash">Delete</a> 
									</td>
								</tr>															
							</tbody>
						</table>
				</div>
		 </div>
	</div>	
@endsection