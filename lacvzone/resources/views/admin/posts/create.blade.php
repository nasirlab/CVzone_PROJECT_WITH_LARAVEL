@extends('admin.layouts.master')
@section('posts_menu_add','active')
@section('pageTitle')
<span class="text-semibold">POSTS - ADD</span>  || <a href="/posts">MY POSTS</a>
@endsection

@section('content')
	<div class="row">
	
			<form action="store.php" method="POST" enctype="multipart/form-data">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">

								<div class="col-md-5">
									<div class="form-group">
										<label>Posts Title</label>
										<input class="form-control " type="tel" placeholder="Learn PHP" name="title">
									</div>	
									<div class="form-group">
										<label>Author name</label>
										<input  class="form-control" type="text" placeholder="Rahim" name="author_name">
									</div>												
									<div class="form-group">
										<label>Categoty name</label>
										<input  class="form-control" type="text" placeholder="Education" name="categories">
									</div>										
									
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control input-xlg"  placeholder="" name="description"></textarea>
									</div>									
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Country Name</label>
										<input class="form-control" type="text" placeholder="Bangladesh" name="country_name">
									</div>					
									<div class="form-group">
										<label>City name</label>
										<input class="form-control" type="text" placeholder="Bangladesh" name="city_name">
									</div>										
									<div class="form-group">
										<label>Tags</label>
										<input class="form-control" type="text" placeholder="php" name="tags">
									</div>										
									<div class="form-group">
										<label>Post Image</label>
										<input class="form-control" type="file" name="img">
									</div>					
								</div>
							</div>
                            
							<div class="form-group">
								<input class="btn " type="submit" value="Save" name="awards">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
	</div>	
@endsection