<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('admin.users.dashboard');
});

Route::get('admin/dashboard', function () {
    return view('admin.users.dashboard');
});

//settings
Route::get('/settings', 'SettingsController@edit');

//Abouts
Route::get('/abouts', 'AboutsController@index');
Route::get('/abouts/edit', 'AboutsController@edit');

//hobies
Route::get('/hobbies', 'HobbiesController@index');
Route::get('/hobbies/create','HobbiesController@create');
Route::get('/hobbies/edit','HobbiesController@edit');

//facts
Route::get('/facts','FactsController@index');
Route::get('/facts/create', 'FactsController@create');
Route::get('/facts/edit','FactsController@edit');


//educations
Route::get('/educations','EducationsController@index');
Route::get('/educations/create', 'EducationsController@create');
Route::get('/educations/edit','EducationsController@edit');

//experiences
Route::get('/experiences','ExperiencesController@index');
Route::get('/experiences/create','ExperiencesController@create');
Route::get('/experiences/edit','ExperiencesController@edit');


//awards
Route::get('/awards','AwardsController@index');
Route::get('/awards/create', 'AwardsController@create');
Route::get('/awards/edit','AwardsController@edit');

//posts
Route::get('/posts','PostsController@index');
Route::get('/posts/create', 'PostsController@create');
Route::get('/posts/show','PostsController@show');
Route::get('/posts/edit','PostsController@edit');


//services
Route::get('/services', function () {
    return view('admin.services.index');
});
Route::get('/services/create', function () {
    return view('admin.services.create');
});
Route::get('/services/edit', function () {
    return view('admin.services.edit');
});
Route::get('/services/show', function () {
    return view('admin.services.show');
});
//teaching
Route::get('/teaching', function () {
    return view('admin.teaching.index');
});
Route::get('/teaching/create', function () {
    return view('admin.teaching.create');
});
Route::get('/teaching/edit', function () {
    return view('admin.teaching.edit');
});
//skills
Route::get('/skills', function () {
    return view('admin.skills.index');
});
Route::get('/skills/create', function () {
    return view('admin.skills.create');
});
Route::get('/skills/edit', function () {
    return view('admin.skills.edit');
});
//portfolios
Route::get('/portfolios', function () {
    return view('admin.portfolios.index');
});
Route::get('/portfolios/create', function () {
    return view('admin.portfolios.create');
});
Route::get('/portfolios/edit', function () {
    return view('admin.portfolios.edit');
});
Route::get('/portfolios/show', function () {
    return view('admin.portfolios.show');
});


//contacts
Route::get('/contacts', function () {
    return view('admin.contacts.index');
});
