<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HobbiesController extends Controller
{
    public function index(){
    	return view('admin.hobbies.index');
    }   
     public function create(){
    	return view('admin.hobbies.create');
    }   
     public function edit(){
     	 return view('admin.hobbies.edit');
    }
}
