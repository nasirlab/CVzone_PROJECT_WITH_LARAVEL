<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function index(){
    	return view('admin.posts.index');
    }     
     public function show(){
    	return view('admin.posts.show');
    }   
     public function create(){
    	return view('admin.posts.create');
    }    
    public function edit(){
    	return view('admin.posts.edit');
    }
}
