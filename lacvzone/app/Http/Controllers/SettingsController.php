<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
   public function edit(){
   	return view('admin.settings.edit');
   }
}
