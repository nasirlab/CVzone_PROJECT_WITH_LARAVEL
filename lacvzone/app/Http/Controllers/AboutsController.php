<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutsController extends Controller
{
    public function index(){
    	   return view('admin.abouts.index');
    }    
    public function edit(){
    	    return view('admin.abouts.edit');
    }
}
